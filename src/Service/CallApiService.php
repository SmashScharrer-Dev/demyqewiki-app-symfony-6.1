<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService {
    public function __construct(
        private readonly HttpClientInterface $demyqewikiClient
    ) {}

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getSummonerAccount(string $summonerName): array
    {
        return $this->demyqewikiClient->request('GET', '/summoner/' . $summonerName)->toArray();
    }

    public function getSummonerRanked(string $summonerName): array
    {
        return $this->demyqewikiClient->request('GET', '/league/' . $summonerName)->toArray();
    }
}