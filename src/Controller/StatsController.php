<?php

namespace App\Controller;

use App\Service\CallApiService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class StatsController extends AbstractController
{
    /**
     * @param string $summonerName
     * @param CallApiService $callApiService
     * @param CacheInterface $cache
     * @return Response
     * @throws InvalidArgumentException
     */
    #[Route('/stats/{summonerName}', name: 'app_stats')]
    public function index(string $summonerName, CallApiService $callApiService, CacheInterface $cache): Response
    {
        $summonerAccountRequest = $cache->get('summoner_' . $summonerName, function(ItemInterface $item) use ($summonerName, $callApiService) {
            $item->expiresAfter(3600);

            return  $callApiService->getSummonerAccount($summonerName);
        });

        $summonerRankedRequest = $cache->get('summoner_ranked_' . $summonerName, function(ItemInterface $item) use ($summonerName, $callApiService) {
            $item->expiresAfter(3600);

            return  $callApiService->getSummonerRanked($summonerName);
        });

        return $this->render('stats/index.html.twig', [
            'summoner_account' => $summonerAccountRequest,
            'summoner_ranked' => $summonerRankedRequest,
        ]);
    }
}
